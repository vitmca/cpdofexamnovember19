package com.agiletestingalliance;
/*
 * Authored by Ashish
 */
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AboutCPDOFTest{

	@Test
	public void testMul() throws Exception {
		String result = new AboutCPDOF().desc();
		assertEquals("Test 1 AboutCPDOFTest class", "CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings.", result);
		
		
  }
  
  }
